class_name PauseMenu extends Control

func _ready() -> void:
	$MarginContainer/ContinueButton.pressed.connect(hide_menu)

func show_menu() -> void:
	get_tree().paused = true
	show()

func hide_menu() -> void:
	hide()
	get_tree().paused = false
