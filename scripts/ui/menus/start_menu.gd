class_name StartMenu extends Control

func _ready() -> void:
	# Make the game unpaused if it was paused during the game
	get_tree().paused = false
	# Show the version of the game
	$GameVersionLabel.text = "Version " + str(ProjectSettings.get_setting("application/config/version"))

## Loads the game scene
func load_game_scene() -> void:
	get_tree().change_scene_to_file("res://scenes/game.tscn")

func _on_player_button_pressed() -> void:
	OptionManager._playing_with_ai = false
	load_game_scene()

func _on_ai_button_pressed() -> void:
	OptionManager._playing_with_ai = true
	load_game_scene()
