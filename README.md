![GitLab License](https://img.shields.io/gitlab/license/SimoneStarace%2FG-Pong?style=flat-square) ![GitLab Tag](https://img.shields.io/gitlab/v/tag/SimoneStarace%2FG-Pong?style=flat-square) ![GitLab Last Commit](https://img.shields.io/gitlab/last-commit/SimoneStarace%2FG-Pong?style=flat-square)

# Table of Contents
1. [Introduction](#introduction)
2. [Software Tools Used](#tools_used)

# Introduction
G-Pong is simply the videogame Pong developed using the Godot game engine.

The reason is simply to learn the engine but recreating an existing game.

# Software Tools Used
- Game Engine
    - [Godot](https://godotengine.org/)
- Sound Effects
    - [sfxr](https://drpetter.se/project_sfxr.html)
    - [jfxr](https://jfxr.frozenfractal.com/)
- Version Control System
    - [git](https://git-scm.com/)
